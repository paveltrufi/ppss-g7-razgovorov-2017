package ppss.ejercicio3;

import org.junit.Test;
import ppss.ejercicio3.excepciones.ReservaException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by pavel on 15/03/17.
 */
public class Reserva_realizaReserva_IntegrationTest {
    private Reserva reserva;
    private String login, password, socio, BD;
    private String[] isbns;
    private ReservaException resultado, resultadoEsperado;

    @Test
    public void realizaReserva_C1() throws Exception {
        reserva = new Reserva();
        login = "xxxx";
        password = "xxxx";
        socio = "Luis";
        isbns = new String[] {"1111"};
        resultadoEsperado = new ReservaException("ERROR de permisos; ");
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
        }
        assertEquals(resultadoEsperado, resultado);
    }

    @Test
    public void realizaReserva_C2() throws Exception {
        reserva = new Reserva();
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"1111", "2222"};
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            fail();
        }
    }

    @Test
    public void realizaReserva_C3() throws Exception {
        reserva = new Reserva();
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"3333"};
        resultadoEsperado = new ReservaException("ISBN invalido:3333; ");
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
        }
        assertEquals(resultadoEsperado, resultado);
    }

    @Test
    public void realizaReserva_C4() throws Exception {
        reserva = new Reserva();
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[] {"1111"};
        resultadoEsperado = new ReservaException("SOCIO invalido; ");
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
        }
        assertEquals(resultadoEsperado, resultado);
    }

    @Test
    public void realizaReserva_C5() throws Exception {
        reserva = new Reserva();
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"1111"};
        resultadoEsperado = new ReservaException("CONEXION invalida; ");
        //No podemos cubrir este caso
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
        }
        assertEquals(resultadoEsperado, resultado);
    }
}