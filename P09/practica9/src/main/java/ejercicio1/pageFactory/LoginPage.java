package ejercicio1.pageFactory;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Pausa;

public class LoginPage {
    WebDriver driver;
    @FindBy(name = "uid")
    WebElement userID;
    @FindBy(name = "password")
    WebElement password;
    @FindBy(name = "btnLogin")
    WebElement loginBtn;
    @FindBy(className = "barone")
    WebElement loginTitle;
    
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.driver.get("http://demo.guru99.com/V4");
    }

    public ManagerPage login(String user, String pass) {
        userID.sendKeys(user);
        password.sendKeys(pass);
        loginBtn.click();
        Pausa.milisegundos(Pausa.DEFAULT_DELAY);
        return PageFactory.initElements(driver, ManagerPage.class);
    }

    public String getLoginTitle() {
        return loginTitle.getText();
    }
    
    public String getAlertTextAndAccept() {
        Alert alert = driver.switchTo().alert();
        final String text = alert.getText();
        alert.accept();
        Pausa.milisegundos(Pausa.DEFAULT_DELAY);
        return text;
    }
}
