package ppss.ejercicio3;

public class OperacionFactory {
    private static IOperacionBO operacionBO = null;

    public static IOperacionBO createOperacion() {
        return operacionBO == null ? new Operacion() : operacionBO;
    }

    public static void setOperacionBO(IOperacionBO operacionBO) {
        OperacionFactory.operacionBO = operacionBO;
    }
}
