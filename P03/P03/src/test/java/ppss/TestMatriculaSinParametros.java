package ppss;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@Category(SinParametros.class)
public class TestMatriculaSinParametros {
    private int edad;
    private boolean familiaNumerosa, repetidor;
    private float tasa, tasaEsperada;
    private Matricula matricula = new Matricula();

    @Test
    public void testC1() {
        edad = 18;
        familiaNumerosa = false;
        repetidor = true;
        tasaEsperada = 2000.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }

    @Test
    public void testC2() {
        edad = 18;
        familiaNumerosa = false;
        repetidor = false;
        tasaEsperada = 500.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }

    @Test
    public void testC3() {
        edad = 18;
        familiaNumerosa = true;
        repetidor = false;
        tasaEsperada = 250.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }

    @Test
    public void testC4() {
        edad = 65;
        familiaNumerosa = false;
        repetidor = false;
        tasaEsperada = 250.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }

    @Test
    public void testC5() {
        edad = 60;
        familiaNumerosa = false;
        repetidor = false;
        tasaEsperada = 400.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }

    @Test
    public void testC6() {
        edad = 60;
        familiaNumerosa = true;
        repetidor = false;
        tasaEsperada = 400.00f;
        tasa = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertThat(tasa, is(tasaEsperada));
    }
}