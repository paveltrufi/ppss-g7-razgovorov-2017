package ppss.ejercicio5;

import org.easymock.EasyMock;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


public class ListadosMockTest {
    @Test
    public void porApellidos_C1() throws Exception {
        ResultSet resultSet = EasyMock.createMock(ResultSet.class);
        EasyMock.expect(resultSet.next())
                .andReturn(true)
                .andReturn(true)
                .andReturn(false);
        EasyMock.expect(resultSet.getString("apellido1"))
                .andReturn("Garcia")
                .andReturn("Lacalle");
        EasyMock.expect(resultSet.getString("apellido2"))
                .andReturn("Molina")
                .andReturn("Verna");
        EasyMock.expect(resultSet.getString("nombre"))
                .andReturn("Ana")
                .andReturn("Jose Luis");

        Statement statement = EasyMock.createMock(Statement.class);
        EasyMock.expect(statement.executeQuery("SELECT apellido1, apellido2, nombre FROM alumnos"))
                .andReturn(resultSet);

        Connection connection = EasyMock.createMock(Connection.class);
        EasyMock.expect(connection.createStatement()).andReturn(statement);

        EasyMock.replay(resultSet, statement, connection);
        String expected = "Garcia, Molina, Ana\nLacalle, Verna, Jose Luis\n";
        String actual = new Listados().porApellidos(connection, "alumnos");
        assertThat(actual, is(expected));
        EasyMock.verify(resultSet, statement, connection);
    }

    @Test
    public void porApellidos_C2() throws Exception {
        ResultSet resultSet = EasyMock.createMock(ResultSet.class);
        EasyMock.expect(resultSet.next())
                .andThrow(new SQLException());

        Statement statement = EasyMock.createMock(Statement.class);
        EasyMock.expect(statement.executeQuery("SELECT apellido1, apellido2, nombre FROM alumnos"))
                .andReturn(resultSet);

        Connection connection = EasyMock.createMock(Connection.class);
        EasyMock.expect(connection.createStatement()).andReturn(statement);

        EasyMock.replay(resultSet, statement, connection);
        Listados listados = new Listados();
        try {
            new Listados().porApellidos(connection, "alumnos");
            fail("Exception not thrown");
        } catch (SQLException ignored) {}
        EasyMock.verify(resultSet, statement, connection);
    }


}