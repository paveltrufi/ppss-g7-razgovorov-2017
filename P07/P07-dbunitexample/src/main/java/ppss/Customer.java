package ppss;

public class Customer {
    private int _id;
    private String _firstName;
    private String _lastName;
    private String _street;
    private String _city;

    public Customer(int id, String firstName, String lastName) {
        _id = id;
        _firstName = firstName;
        _lastName = lastName;
    }

    public int getId() {
        return _id;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String getStreet() {
        return _street;
    }

    public String getCity() {
        return _city;
    }

    public void setStreet(String street) {
        _street = street;
    }

    public void setCity(String city) {
        _city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (_id != customer._id) return false;
        if (_firstName != null ? !_firstName.equals(customer._firstName) : customer._firstName != null) return false;
        if (_lastName != null ? !_lastName.equals(customer._lastName) : customer._lastName != null) return false;
        if (_street != null ? !_street.equals(customer._street) : customer._street != null) return false;
        return _city != null ? _city.equals(customer._city) : customer._city == null;
    }
}
