package ppss.ejercicio1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import static org.junit.Assert.*;

public class MultipathExampleTest {
    MultipathExample me = new MultipathExample();
    
    @Test
    public void multiPath_C1() {
        assertThat(me.multiPath(10, 10, 0), CoreMatchers.is(20));
    }
    
    @Test
    public void multiPath_C2() {
        assertThat(me.multiPath(0, 0, 0), CoreMatchers.is(0));
    }
}
