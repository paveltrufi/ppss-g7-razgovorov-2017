package utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Pausa {
    public static final int DEFAULT_DELAY = 1500;
    public static void milisegundos(long milisegundos) {
        try {
            Thread.sleep(milisegundos);
        } catch (InterruptedException ex) {
            Logger.getLogger(Pausa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
