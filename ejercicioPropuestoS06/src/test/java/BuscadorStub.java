public class BuscadorStub extends Buscador {
    private int elemPendientes;

    @Override
    public int elemPendientes(Cliente cli) {
        return elemPendientes;
    }

    public void setElemPendientes(int elemPendientes) {
        this.elemPendientes = elemPendientes;
    }
}
