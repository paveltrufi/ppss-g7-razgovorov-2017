package ppss.ejercicio1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import static org.junit.Assert.*;

public class HandleStringsTest {
    HandleStrings instance = new HandleStrings();

    @Test
    public void testExtractMiddle() {
        assertThat(instance.extractMiddle(":test:"), CoreMatchers.is("test"));
    }
}
