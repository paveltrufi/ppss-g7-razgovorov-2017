public class Factura {
    private String idCliente;
    private float total_factura;

    public Factura() {
    }

    public Factura(String idCliente) {
        this.idCliente = idCliente;
        this.total_factura = 0.0f;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public float getTotal_factura() {
        return total_factura;
    }

    public void setTotal_factura(float total_factura) {
        this.total_factura = total_factura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Factura)) return false;

        Factura factura = (Factura) o;

        if (Float.compare(factura.getTotal_factura(), getTotal_factura()) != 0) return false;
        return getIdCliente() != null ? getIdCliente().equals(factura.getIdCliente()) : factura.getIdCliente() == null;
    }

    @Override
    public int hashCode() {
        return getIdCliente() != null ? getIdCliente().hashCode() : 0;
    }
}
