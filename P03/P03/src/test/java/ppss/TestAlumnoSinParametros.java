package ppss;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Category(SinParametros.class)
public class TestAlumnoSinParametros {
    String NIF;
    boolean valido, validoEsperado;
    Alumno alumno = new Alumno();

    @Test
    public void testC1() {
        NIF = "74012863J";
        validoEsperado = true;
        valido = alumno.validaNif(NIF);
        assertThat(valido, is(validoEsperado));
    }

    @Test
    public void testC1_2() {
        NIF = "74012863K";
        validoEsperado = false;
        valido = alumno.validaNif(NIF);
        assertThat(valido, is(validoEsperado));
    }

    @Test
    public void testC2() {
        NIF = "-7401286J";
        validoEsperado = false;
        valido = alumno.validaNif(NIF);
        assertThat(valido, is(validoEsperado));
    }

    @Test
    public void testC3() {
        NIF = "740J12863";
        validoEsperado = false;
        valido = alumno.validaNif(NIF);
        assertThat(valido, is(validoEsperado));
    }

    //C4 Caso imposible

    @Test
    public void testC5() {
        NIF = "74012863";
        validoEsperado = false;
        valido = alumno.validaNif(NIF);
        assertThat(valido, is(validoEsperado));
    }
}