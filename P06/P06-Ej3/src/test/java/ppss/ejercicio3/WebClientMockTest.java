package ppss.ejercicio3;

import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class WebClientMockTest {
    private static final String URL_InputStream_OK = "Funciona";
    private URL url;
    private boolean connectionFail;
    private String expected, actual;

    public WebClientMockTest(URL url, boolean connectionFail, String expected) {
        this.url = url;
        this.connectionFail = connectionFail;
        this.expected = expected;
    }

     @Parameters(name = "getContent_C{index} (url: {0}, fallo_conexion: {1}, esperado: {2})")
     public static Collection<Object[]> data() throws MalformedURLException {
         return Arrays.asList(new Object[][] {
                 {new URL("http://www.ua.es"), false, URL_InputStream_OK},
                 {new URL("http://www.ua.es"), true, null},
         });
     }

    @Test
    public void getContent() throws Exception {
        HttpURLConnection httpURLConnection = EasyMock.partialMockBuilder(HttpURLConnection.class)
                .addMockedMethod("getInputStream")
                .createMock();
        if (connectionFail) {
            EasyMock.expect(httpURLConnection.getInputStream()).andThrow(new IOException());
        } else {
            EasyMock.expect(httpURLConnection.getInputStream()).andReturn(new ByteArrayInputStream(URL_InputStream_OK.getBytes()));
        }

        WebClient webClient = EasyMock.partialMockBuilder(WebClient.class)
                .addMockedMethod("createHttpURLConnection")
                .createMock();
        EasyMock.expect(webClient.createHttpURLConnection(url)).andReturn(httpURLConnection);

        EasyMock.replay(httpURLConnection, webClient);
        actual = webClient.getContent(url);
        assertThat(actual, is(expected));
        EasyMock.verify(httpURLConnection, webClient);
    }

}