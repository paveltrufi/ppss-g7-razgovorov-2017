package ejercicio2.pageFactory;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import utils.Pausa;

public class NewCustomerPageTest {

    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManager;
    NewCustomerPage poNewCustomer;
    DeleteCustomerPage poDeleteCustomer;
    private final String user = "mngr77423", pass = "pesUqun";
    private final String name = "pr";
    private final char gender = 'm';
    private final String dateOfBirth = "27/11/1996";
    private final String address = "Calle X";
    private final String city = "Alicante";
    private final String state = "Alicante";
    private final String pin = "123456";
    private final String telephone = "999999999";
    private final String mail = "joe" + getId() + "@example.com";
    private final String password = "123456";

    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void addNewCustomerTest() {
        login();
        poNewCustomer = poManager.newCustomer();
        assertTrue(poNewCustomer.getNewCustomerTitleText().contains("Add New Customer"));
        poNewCustomer.addNewCustomer(name, gender, dateOfBirth, address, city, state, pin, telephone, mail, password);
        final String id = poNewCustomer.getCustomerId();
        assertTrue(poNewCustomer.customerRegisteredText.contains("Customer Registered Successfully!!!"));
        poDeleteCustomer = poManager.deleteCustomer();
        assertTrue(poDeleteCustomer.getDeleteCustomerHeadingText().contains("Delete Customer Form"));
        poDeleteCustomer.deleteCustomer(id);
        assertTrue(AlertPage.getAlertTextAndAccept(poDeleteCustomer.driver).contains("Do you really want to delete this Customer?"));
        assertTrue(AlertPage.getAlertTextAndAccept(poDeleteCustomer.driver).contains("Customer deleted Successfully"));
    }

    @Test
    public void addSameCustomerTest() {
        login();
        poNewCustomer = poManager.newCustomer();
        assertTrue(poNewCustomer.getNewCustomerTitleText().contains("Add New Customer"));
        poNewCustomer.addNewCustomer(name, gender, dateOfBirth, address, city, state, pin, telephone, mail, password);
        final String id = poNewCustomer.getCustomerId();
        assertTrue(poNewCustomer.customerRegisteredText.contains("Customer Registered Successfully!!!"));
        poNewCustomer = poManager.newCustomer();
        assertTrue(poNewCustomer.getNewCustomerTitleText().contains("Add New Customer"));
        poNewCustomer.addNewCustomer(name, gender, dateOfBirth, address, city, state, pin, telephone, mail, password);
        assertTrue(AlertPage.getAlertTextAndAccept(poNewCustomer.driver).contains("Email Address Already Exist !!"));
        poDeleteCustomer = poManager.deleteCustomer();
        assertTrue(poDeleteCustomer.getDeleteCustomerHeadingText().contains("Delete Customer Form"));
        poDeleteCustomer.deleteCustomer(id);
        assertTrue(AlertPage.getAlertTextAndAccept(poDeleteCustomer.driver).contains("Do you really want to delete this Customer?"));
        assertTrue(AlertPage.getAlertTextAndAccept(poDeleteCustomer.driver).contains("Customer deleted Successfully"));
    }

    public void login() {
        assertTrue(poLogin.getLoginTitle().contains("Guru99 Bank"));
        poManager = poLogin.login(user, pass);
        assertTrue(poManager.getHomePageDashboardUserName().contains("Manger Id : " + user));
    }

    /**
     * Devuelve un ID aleatorio. Para que en cada driver se cree un correo
     * diferente y no tenga que estar borrándolos a mano, si es que el propio
     * driver no lo borra
     *
     * @return
     */
    public String getId() {
        return Integer.toString(ThreadLocalRandom.current().nextInt(0, 999999));
    }
}
