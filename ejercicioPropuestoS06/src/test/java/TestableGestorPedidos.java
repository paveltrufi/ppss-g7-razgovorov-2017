public class TestableGestorPedidos extends GestorPedidos {
    private Buscador buscador;

    @Override
    public Buscador getBuscador() {
        return buscador;
    }

    public void setBuscador(Buscador buscador) {
        this.buscador = buscador;
    }
}
