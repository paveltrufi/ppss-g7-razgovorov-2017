package ppss;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Alumno {
    public boolean validaNif(String nif) {
        if (nif.length() != 9) {
            return false;
        }

        String dni = nif.substring(0, 8);
        char letra = nif.charAt(8);

        //A partir de una expresión regular, obtenemos una representación
        //compilada (objeto de tipo Pattern)
        Pattern pattern = Pattern.compile("[0-9]{8}");
        //El patrón obtenido se utiliza para crear un objeto Matcher
        Matcher matcher = pattern.matcher(dni);
        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";

        long ldni = 0;
        try {
            ldni = Long.parseLong(dni);
        } catch (NumberFormatException e) {
            return false;
        }

        int indice = (int)(ldni % 23);
        if (indice < 0) return false;
        char letraEsperada = letras.charAt(indice);

        //El método matches() devuelve true si el emparejamiento del patrón
        //ha tenido éxito, y false en caso contrario
        return matcher.matches() && letra == letraEsperada;
    }
}
