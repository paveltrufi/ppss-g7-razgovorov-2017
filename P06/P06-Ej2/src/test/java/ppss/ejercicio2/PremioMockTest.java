package ppss.ejercicio2;

import org.easymock.EasyMock;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PremioMockTest {
    private String premio, premioEsperado;

    @Test
    public void compruebaPremio_C1() throws Exception {
        ClienteWebService clienteWebService = EasyMock.createMock(ClienteWebService.class);
        EasyMock.expect(clienteWebService.obtenerPremio()).andReturn("pez de goma");

        Premio premio = EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(premio.generaNumero()).andReturn(0.01f);
        premio.cliente = clienteWebService;

        EasyMock.replay(clienteWebService);
        EasyMock.replay(premio);

        premioEsperado = "Premiado con pez de goma";
        this.premio = premio.compruebaPremio();
        assertThat(premioEsperado, is(this.premio));
        EasyMock.verify(clienteWebService, premio);
    }

    @Test
    public void compruebaPremio_C2() throws Exception {
        ClienteWebService clienteWebService = EasyMock.createMock(ClienteWebService.class);
        EasyMock.expect(clienteWebService.obtenerPremio()).andThrow(new ClienteWebServiceException());

        Premio premio = EasyMock.createMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(premio.generaNumero()).andReturn(0.01f);
        premio.cliente = clienteWebService;

        EasyMock.replay(clienteWebService);
        EasyMock.replay(premio);

        premioEsperado = "Premiado";
        this.premio = premio.compruebaPremio();
        assertThat(this.premio, is(premioEsperado));
        EasyMock.verify(clienteWebService, premio);
    }
}