package ppss.ejercicio1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class GestorLlamadas_calculaConsumoTest {
    @Parameters(name = "calculaConsumo_C{index} (minuto: {0}, hora: {1}, esperado: {2})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {10, 15, 208d},
                {10, 22, 105d},
        });
    }

    private int minutos, hora;
    double consumo, consumoEsperado;

    public GestorLlamadas_calculaConsumoTest(int minutos, int hora, double consumoEsperado) {
        this.minutos = minutos;
        this.hora = hora;
        this.consumoEsperado = consumoEsperado;
    }

    @Test
    public void calculaConsumo() throws Exception {
        TestableGestorLlamadas gestorLlamadas = new TestableGestorLlamadas();
        gestorLlamadas.setHora(hora);
        consumo = gestorLlamadas.calculaConsumo(minutos);
        assertEquals(consumoEsperado, consumo);

    }
}