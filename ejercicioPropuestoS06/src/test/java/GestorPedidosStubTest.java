import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class GestorPedidosStubTest {
    private Cliente cliente;
    private int numElem;
    private Factura facturaEsperada, facturaActual;
    private TestableGestorPedidos testableGestorPedidos;
    private BuscadorStub buscadorStub;
    private FacturaException excepcionEsperada;

    @Test
    public void generarFactura_C1() throws Exception {
        cliente = new Cliente("cliente1", 20);
        numElem = 10;

        buscadorStub = new BuscadorStub();
        buscadorStub.setElemPendientes(numElem);
        testableGestorPedidos = new TestableGestorPedidos();
        testableGestorPedidos.setBuscador(buscadorStub);

        facturaEsperada = new Factura("cliente1");
        facturaEsperada.setTotal_factura(200);
        facturaActual = testableGestorPedidos.generarFactura(cliente);
        assertThat(facturaActual, is(facturaEsperada));
    }

    @Test
    public void generarFactura_C2() throws Exception {
        cliente = new Cliente("cliente1", 20);
        numElem = 0;

        buscadorStub = new BuscadorStub();
        buscadorStub.setElemPendientes(numElem);
        testableGestorPedidos = new TestableGestorPedidos();
        testableGestorPedidos.setBuscador(buscadorStub);

        excepcionEsperada = new FacturaException("No hay nada pendiente de facturar");
        try {
            testableGestorPedidos.generarFactura(cliente);
            fail("Exception not thrown");
        } catch (FacturaException e) {
            assertThat(e.getMessage(), is(excepcionEsperada.getMessage()));
        }
    }
}