package ppss.ejercicio4;

import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

public interface IOperacionBO {
    void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, SocioInvalidoException, JDBCException;
}
