package ppss.ejercicio4;

import org.easymock.EasyMock;
import org.junit.Test;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class ReservaMockTest {
    private String login, password, socio;
    private String[] isbns;
    private ReservaException resultado, resultadoEsperado;

    @Test
    public void realizaReserva_C1() throws Exception {
        login = password = "xxxx";
        socio = "Luis";
        isbns = new String[]{"11111"};
        resultadoEsperado = new ReservaException("ERROR de permisos; ");

        Reserva reserva = createReservaMock(new String[]{"compruebaPermisos"}, false);

        EasyMock.replay(reserva);
        comprobarMensajesRealizarReserva(reserva);
        EasyMock.verify(reserva);
    }

    @Test
    public void realizaReserva_C2() throws Exception {
        login = password = "ppss";
        socio = "Luis";
        isbns = new String[]{"11111", "22222"};
        resultadoEsperado = null;

        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);

        FactoriaBOs fd = EasyMock.createMock(FactoriaBOs.class);
        EasyMock.expect(fd.getOperacionBO()).andReturn(io);

        Reserva reserva = createReservaMock(new String[]{"compruebaPermisos", "getFactoriaBOs"}, true);
        EasyMock.expect(reserva.getFactoriaBOs()).andReturn(fd);

        EasyMock.replay(fd, reserva);
        reserva.realizaReserva(login, password, socio, isbns);
        EasyMock.verify(fd, reserva);
    }

    @Test
    public void realizaReserva_C3() throws Exception {
        login = password = "ppss";
        socio = "Luis";
        isbns = new String[]{"33333"};
        resultadoEsperado = new ReservaException("ISBN invalido:33333; ");

        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        io.operacionReserva(socio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());

        FactoriaBOs fd = EasyMock.createMock(FactoriaBOs.class);
        EasyMock.expect(fd.getOperacionBO()).andReturn(io);

        Reserva reserva = createReservaMock(new String[]{"compruebaPermisos", "getFactoriaBOs"}, true);
        EasyMock.expect(reserva.getFactoriaBOs()).andReturn(fd);

        EasyMock.replay(io, fd, reserva);
        comprobarMensajesRealizarReserva(reserva);
        EasyMock.verify(io, fd, reserva);
    }

    @Test
    public void realizaReserva_C4() throws Exception {
        login = password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"11111"};
        resultadoEsperado = new ReservaException("SOCIO invalido; ");

        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        io.operacionReserva(socio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new SocioInvalidoException());

        FactoriaBOs fd = EasyMock.createMock(FactoriaBOs.class);
        EasyMock.expect(fd.getOperacionBO()).andReturn(io);

        Reserva reserva = createReservaMock(new String[]{"compruebaPermisos", "getFactoriaBOs"}, true);
        EasyMock.expect(reserva.getFactoriaBOs()).andReturn(fd);

        EasyMock.replay(io, fd, reserva);
        comprobarMensajesRealizarReserva(reserva);
        EasyMock.verify(io, fd, reserva);
    }

    @Test
    public void realizaReserva_C5() throws Exception {
        login = password = "ppss";
        socio = "Luis";
        isbns = new String[]{"11111"};
        resultadoEsperado = new ReservaException("CONEXION invalida; ");

        IOperacionBO io = EasyMock.createMock(IOperacionBO.class);
        io.operacionReserva(socio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new JDBCException());

        FactoriaBOs fd = EasyMock.createMock(FactoriaBOs.class);
        EasyMock.expect(fd.getOperacionBO()).andReturn(io);

        Reserva reserva = createReservaMock(new String[]{"compruebaPermisos", "getFactoriaBOs"}, true);
        EasyMock.expect(reserva.getFactoriaBOs()).andReturn(fd);

        EasyMock.replay(io, fd, reserva);
        comprobarMensajesRealizarReserva(reserva);
        EasyMock.verify(io, fd, reserva);
    }

    private Reserva createReservaMock(String[] methods, boolean granted) {
        Reserva reserva = EasyMock.createMockBuilder(Reserva.class).addMockedMethods(methods).createMock();
        EasyMock.expect(
                reserva.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))
                .andReturn(granted);
        return reserva;
    }

    private void comprobarMensajesRealizarReserva(Reserva reserva) {
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
            assertThat(resultado.getMessage(), is(resultadoEsperado.getMessage()));
        }
        if (resultado == null) assertNull(resultadoEsperado); //No lanza excepción; sólo OK si no esperaba excepción
    }
}