package ppss.ejercicio3;

import ppss.ejercicio3.excepciones.IsbnInvalidoException;
import ppss.ejercicio3.excepciones.JDBCException;
import ppss.ejercicio3.excepciones.SocioInvalidoException;

import java.util.Arrays;
import java.util.List;

public class OperacionStub implements IOperacionBO {
    private final static List<String> socios = Arrays.asList("Luis");
    private final static List<String> isbns = Arrays.asList("1111", "2222");
    private final String DB_OK;

    public OperacionStub(String DB_OK) {
        this.DB_OK = DB_OK;
    }

    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
        if (DB_OK.equals("OK")) {
            if (socios.contains(socio)) {
                if (!isbns.contains(isbn)) {
                    throw new IsbnInvalidoException();
                }
            } else {
                throw new SocioInvalidoException();
            }
        } else {
            throw new JDBCException();
        }
    }
}
