package ppss;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class DataArrayTest {
    @Test
    public void testDelete1() {
        DataArray instance = new DataArray();
        int[] data = {1, 2, 3, 4};
        int elem = 1;
        int[] expResult = {2, 3, 4, 0, 0, 0, 0, 0, 0, 0};
        int expSize = 3;
        
        for (int d : data) instance.add(d);
        int[] result = instance.delete(elem);
        assertArrayEquals(expResult, result);
        int resultSize = instance.size();
        assertEquals(expSize, resultSize);
    }
    
    @Test
    public void testDelete2() {
        DataArray instance = new DataArray();
        int[] data = {1, 1, 1, 1, 1};
        int elem = 0;
        int[] expResult = {1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
        int expSize = 5;
        
        for (int d : data) instance.add(d);
        int[] result = instance.delete(elem);
        assertArrayEquals(expResult, result);
        int resultSize = instance.size();
        assertEquals(expSize, resultSize);
    }
    
    @Test
    public void testDelete3() {
        DataArray instance = new DataArray();
        int[] data = {1, 1, 1, 1, 1};
        int elem = 1;
        int[] expResult = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int expSize = 0;
        
        for (int d : data) instance.add(d);
        int[] result = instance.delete(elem);
        assertArrayEquals(expResult, result);
        int resultSize = instance.size();
        assertEquals(expSize, resultSize);
    }
    
    @Test
    public void testDelete4() {
        DataArray instance = new DataArray();
        int[] data = {};
        int elem = 0;
        int[] expResult = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int expSize = 0;
        
        for (int d : data) instance.add(d);
        int[] result = instance.delete(elem);
        assertArrayEquals(expResult, result);
        int resultSize = instance.size();
        assertEquals(expSize, resultSize);
    }
    
    @Test
    public void testDelete5() {
        DataArray instance = new DataArray();
        int[] data = {1};
        int elem = 1;
        int[] expResult = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int expSize = 0;
        
        for (int d : data) instance.add(d);
        int[] result = instance.delete(elem);
        assertArrayEquals(expResult, result);
        int resultSize = instance.size();
        assertEquals(expSize, resultSize);
    }
}
