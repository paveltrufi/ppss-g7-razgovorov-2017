import org.easymock.EasyMock;
import org.junit.Test;

import static org.easymock.EasyMock.anyObject;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class GestorPedidosEasyMockStubTest {
    private Cliente cliente;
    private int numElem;
    private Factura facturaEsperada, facturaActual;
    private GestorPedidos gestorPedidos;
    private Buscador buscador;
    private FacturaException excepcionEsperada;

    @Test
    public void generarFactura_C1() throws Exception {
        cliente = new Cliente("cliente1", 20);
        numElem = 10;

        buscador = EasyMock.createNiceMock(Buscador.class);
        EasyMock.expect(buscador.elemPendientes((Cliente) anyObject())).andStubReturn(numElem);
        gestorPedidos = EasyMock.createMockBuilder(GestorPedidos.class).addMockedMethod("getBuscador").createNiceMock();
        EasyMock.expect(gestorPedidos.getBuscador()).andStubReturn(buscador);

        EasyMock.replay(buscador, gestorPedidos);
        facturaEsperada = new Factura("cliente1");
        facturaEsperada.setTotal_factura(200);
        facturaActual = gestorPedidos.generarFactura(cliente);
        assertThat(facturaActual, is(facturaEsperada));
    }

    @Test
    public void generarFactura_C2() throws Exception {
        cliente = new Cliente("cliente1", 20);
        numElem = 0;

        buscador = EasyMock.createNiceMock(Buscador.class);
        EasyMock.expect(buscador.elemPendientes((Cliente) anyObject())).andStubReturn(numElem);
        gestorPedidos = EasyMock.createMockBuilder(GestorPedidos.class).addMockedMethod("getBuscador").createNiceMock();
        EasyMock.expect(gestorPedidos.getBuscador()).andStubReturn(buscador);

        EasyMock.replay(buscador, gestorPedidos);

        excepcionEsperada = new FacturaException("No hay nada pendiente de facturar");
        try {
            gestorPedidos.generarFactura(cliente);
            fail("Exception not thrown");
        } catch (FacturaException e) {
            assertThat(e.getMessage(), is(excepcionEsperada.getMessage()));
        }
    }
}