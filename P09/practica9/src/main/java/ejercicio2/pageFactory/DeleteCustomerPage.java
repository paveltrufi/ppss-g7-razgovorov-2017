package ejercicio2.pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Pausa;

public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(css = "p.heading3")
    WebElement deleteCustomerHeading;
    @FindBy(name = "cusid")
    WebElement customerId;
    @FindBy(css = "input[type='submit']")
    WebElement submit;

    public DeleteCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getDeleteCustomerHeadingText() {
        return deleteCustomerHeading.getText();
    }
    
    public void deleteCustomer(String id) {
        customerId.sendKeys(id);
        submit.click();
        Pausa.milisegundos(Pausa.DEFAULT_DELAY);
    }
}
