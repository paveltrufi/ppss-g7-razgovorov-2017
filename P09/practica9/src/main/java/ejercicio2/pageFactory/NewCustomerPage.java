package ejercicio2.pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Pausa;

public class NewCustomerPage {

    WebDriver driver;
    @FindBy(css = "p.heading3")
    WebElement newCustomerTitle;
    @FindBy(name = "name")
    WebElement name;
    @FindBy(css = "input[value='m']")
    WebElement maleRadio;
    @FindBy(css = "input[value='f']")
    WebElement femaleRadio;
    @FindBy(name = "dob")
    WebElement dateOfBirth;
    @FindBy(name = "addr")
    WebElement address;
    @FindBy(name = "city")
    WebElement city;
    @FindBy(name = "state")
    WebElement state;
    @FindBy(name = "pinno")
    WebElement pin;
    @FindBy(name = "telephoneno")
    WebElement telephone;
    @FindBy(name = "emailid")
    WebElement email;
    @FindBy(name = "password")
    WebElement password;
    @FindBy(name = "sub")
    WebElement submit;
    @FindBy(name = "res")
    WebElement reset;

    @FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p")
    WebElement customerRegisteredTitleWeb;
    @FindBy(xpath = "//table[@id='customer']/tbody/tr[4]/td[2]")
    WebElement customerIdWeb;
    @FindBy(linkText = "Delete Customer")
    WebElement deleteCustomer;
    String customerRegisteredText, customerId;

    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getNewCustomerTitleText() {
        return newCustomerTitle.getText();
    }
    
    public void addNewCustomer(String name, char gender, String dateOfBirth,
            String address, String city, String state, String pin,
            String telephone, String email, String password) {
        this.name.sendKeys(name);
        if (gender == 'm') {
            this.maleRadio.click();
        } else {
            this.femaleRadio.click();
        }
        this.dateOfBirth.sendKeys(dateOfBirth);
        this.address.sendKeys(address);
        this.city.sendKeys(city);
        this.state.sendKeys(state);
        this.pin.sendKeys(pin);
        this.telephone.sendKeys(telephone);
        this.email.sendKeys(email);
        this.password.sendKeys(password);
        
        this.submit.click();
        Pausa.milisegundos(Pausa.DEFAULT_DELAY);
    }
    
    public String getCustomerId() {
        this.customerRegisteredText = this.customerRegisteredTitleWeb.getText();
        this.customerId = this.customerIdWeb.getText();
        return this.customerId;
    }
}