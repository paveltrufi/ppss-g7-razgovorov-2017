package ppss;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Category(ConParametros.class)
@RunWith(Parameterized.class)
public class TestAlumnoConParametros {
    private String nif;
    private boolean validoEsperado;
    private Alumno alumno = new Alumno();

    public TestAlumnoConParametros(String nif, boolean validoEsperado) {
        this.nif = nif;
        this.validoEsperado = validoEsperado;
    }

    @Parameterized.Parameters(name = "C{index}: validaNif({0})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"74012863J", true},
                {"74012863K", false},
                {"-7401286J", false},
                {"740J1286", false},
                {"74012863", false},
        });
    }

    @Test
    public void TestAlumnoConParametros() {
        assertThat(alumno.validaNif(nif), is(validoEsperado));
    }
}