package ejercicio2.pageFactory;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import utils.Pausa;

/**
 * Para cuando quiera recoger un texto de una alerta y darle a aceptar, uso esto
 * @author ppss
 */
public class AlertPage {
    public static String getAlertTextAndAccept(WebDriver driver) {
        Alert alert = driver.switchTo().alert();
        final String text = alert.getText();
        alert.accept();
        Pausa.milisegundos(Pausa.DEFAULT_DELAY);
        return text;
    }
}
