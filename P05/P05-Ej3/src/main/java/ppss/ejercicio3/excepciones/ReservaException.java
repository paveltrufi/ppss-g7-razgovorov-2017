package ppss.ejercicio3.excepciones;

public class ReservaException extends Exception {
    public ReservaException(String message) {
        super(message);
    }
}
