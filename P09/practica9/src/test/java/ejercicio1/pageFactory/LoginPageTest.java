/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.pageFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class LoginPageTest {
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;
    final String user = "mngr77423", pass = "pesUqun";
    
    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testLogin_Correct() {
        assertTrue(poLogin.getLoginTitle().toLowerCase().contains("guru99 bank"));
        poManagerPage = poLogin.login(user, pass);
        assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id : " + user));
    }
    
    @Test
    public void testLogin_Incorrect() {
        assertTrue(poLogin.getLoginTitle().toLowerCase().contains("guru99 bank"));
        poLogin.login(pass, user); //Me equivoco y pongo el pass en el user y el user en el pass
        assertTrue(poLogin.getAlertTextAndAccept().contains("User or Password is not valid"));
    }
}
