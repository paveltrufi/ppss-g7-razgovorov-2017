package ppss.ejercicio2;

public class TestableGestorLlamadas extends GestorLlamadas {
    private CalendarioStub calendario;

    @Override
    public Calendario getCalendario() {
        return calendario;
    }

    public void setCalendario(CalendarioStub calendario) {
        this.calendario = calendario;
    }
}
