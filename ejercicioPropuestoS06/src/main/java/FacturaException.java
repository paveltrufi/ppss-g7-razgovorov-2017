/**
 * Class description...
 * Created by pavel on 29/03/17.
 */
public class FacturaException extends Exception {
    public FacturaException(String message) {
        super(message);
    }
}
