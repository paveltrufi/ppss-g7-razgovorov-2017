package ppss.ejercicio2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.ejercicio1.*;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class GestorLlamadas_calculaConsumoTest {
    @Parameterized.Parameters(name = "calculaConsumo_C{index} (minuto: {0}, hora: {1}, esperado: {2})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {10, 15, 208d},
                {10, 22, 105d},
        });
    }

    private int minutos, hora;
    double consumo, consumoEsperado;

    public GestorLlamadas_calculaConsumoTest(int minutos, int hora, double consumoEsperado) {
        this.minutos = minutos;
        this.hora = hora;
        this.consumoEsperado = consumoEsperado;
    }

    @Test
    public void calculaConsumo() throws Exception {
        TestableGestorLlamadas gestorLlamadas = new TestableGestorLlamadas();
        CalendarioStub calendarioStub = new CalendarioStub();
        calendarioStub.setHoraActual(hora);
        gestorLlamadas.setCalendario(calendarioStub);
        consumo = gestorLlamadas.calculaConsumo(minutos);
        assertEquals(consumoEsperado, consumo);
    }

}