package ppss.matriculacion.dao;

import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.BasicConfigurator;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ppss.matriculacion.to.AlumnoTO;


/**
 *
 * @author ppss
 */
public class JDBCAlumnoDAOIT {
    private final String
            driverClass = "com.mysql.jdbc.Driver",
            connectionUrl = "jdbc:mysql://localhost:3306/matriculacion?autoReconnect=true&useSSL=false",
            username = "root",
            password = "ppss",
            tableName = "alumnos";
    private AlumnoTO alumno;
    private IDatabaseTester databaseTester;
    private FactoriaDAO factoriaDAO;
    
    @BeforeClass
    public static void setUpClass() {
        BasicConfigurator.configure();
    }
    
    @Before
    public void setUp() throws Exception {
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet dataSet = loader.load("/tabla2.xml");
        alumno = new AlumnoTO();
        databaseTester = new JdbcDatabaseTester(driverClass, connectionUrl, username, password);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        factoriaDAO = new FactoriaDAO();
    }

    @Test
    public void testA1() throws Exception {
        alumno.setNif("33333333C");
        alumno.setNombre("Elena Aguirre Juarez");
        alumno.setFechaNacimiento(getDate(1985, 2, 22));
        factoriaDAO.getAlumnoDAO().addAlumno(alumno);
        
        ITable actualTable = getAlumnosActualTable();
        ITable expectedTable = getAlumnosExpectedTable("/tabla3.xml");
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @Test
    public void testA2() throws Exception {
        alumno.setNif("11111111A");
        alumno.setNombre("Alfonso Ramirez Ruiz");
        alumno.setFechaNacimiento(getDate(1982, 2, 22));
        
        try {
            factoriaDAO.getAlumnoDAO().addAlumno(alumno);
            fail("Exception not thrown");
        } catch (DAOException e) {}
    }
    
    @Test
    public void testA3() throws Exception {
        alumno.setNif("44444444D");
        alumno.setNombre(null);
        alumno.setFechaNacimiento(getDate(1982, 2, 22));
        
        try {
            factoriaDAO.getAlumnoDAO().addAlumno(alumno);
            fail("Exception not thrown");
        } catch (DAOException e) {}
    }
    
    @Test
    public void testA4() throws Exception {
        alumno = null;
        
        try {
            factoriaDAO.getAlumnoDAO().addAlumno(alumno);
            fail("Exception not thrown");
        } catch (DAOException e) {}
    }
    
    @Test
    public void testA5() throws Exception {
        alumno.setNif(null);
        alumno.setNombre("Pedro Garcia Lopez");
        alumno.setFechaNacimiento(getDate(1982, 2, 22));
        
        try {
            factoriaDAO.getAlumnoDAO().addAlumno(alumno);
            fail("Exception not thrown");
        } catch (DAOException e) {}
    }
    
    @Test
    public void testB1() throws Exception {
        factoriaDAO.getAlumnoDAO().delAlumno("11111111A");
        
        ITable actualTable = getAlumnosActualTable();
        ITable expectedTable = getAlumnosExpectedTable("/tabla4.xml");
        Assertion.assertEquals(expectedTable, actualTable);
    }
    
    @Test
    public void testB2() throws Exception {
        try {
            factoriaDAO.getAlumnoDAO().delAlumno("33333333C");
        } catch (DAOException e) {}
    }
    
    private ITable getAlumnosExpectedTable(String xmlFileName) throws DataSetException {
        return ((DataFileLoader) new FlatXmlDataFileLoader())
                .load(xmlFileName)
                .getTable(tableName);
    }
    
    private ITable getAlumnosActualTable() throws Exception {
        IDatabaseConnection connection = databaseTester.getConnection();
        connection.getConfig().setProperty("http://www.dbunit.org/properties/datatypeFactory", new MySqlDataTypeFactory());
        ITable actualTable = connection.createDataSet().getTable(tableName);
        return actualTable;
    }
    
    private Date getDate(int y, int m, int d) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, y);
        cal.set(Calendar.MONTH, m-1); // Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, d);
        Date date = cal.getTime();
        return date;
    }
}
