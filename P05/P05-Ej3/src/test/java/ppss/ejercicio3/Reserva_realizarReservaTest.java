package ppss.ejercicio3;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import ppss.ejercicio3.excepciones.ReservaException;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@RunWith(Parameterized.class)
public class Reserva_realizarReservaTest {
    private final static ReservaException reservaException1 = new ReservaException("ERROR de permisos; ");
    private final static ReservaException reservaException2 = new ReservaException("ISBN invalido:3333; ");
    private final static ReservaException reservaException3 = new ReservaException("SOCIO invalido; ");
    private final static ReservaException reservaException4 = new ReservaException("CONEXION invalida; ");
    private String login, password, socio, BD;
    private String[] isbns;
    private ReservaException resultado = null;
    private ReservaException resultadoEsperado;
    public Reserva_realizarReservaTest(String login, String password, String socio, String BD, String[] isbns, ReservaException resultadoEsperado) {
        this.login = login;
        this.password = password;
        this.socio = socio;
        this.BD = BD;
        this.isbns = isbns;
        this.resultadoEsperado = resultadoEsperado;
    }

    @Parameters(name = "realizarReserva_C{index} (login: {0}, password: {1}, socio: {2}, BD: {3}, isbns: [{4}] = {5}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"xxxx", "xxxx", "Luis", "OK", new String[] {"1111"}, reservaException1},
                {"ppss", "ppss", "Luis", "OK", new String[] {"1111", "2222"}, null},
                {"ppss", "ppss", "Luis", "OK", new String[] {"3333"}, reservaException2},
                {"ppss", "ppss", "Pepe", "OK", new String[] {"1111"}, reservaException3},
                {"ppss", "ppss", "Luis", "fallo", new String[] {"1111"}, reservaException4},
        });
    }

    @Test
    public void realizaReserva() throws Exception {
        Reserva reserva = new TestableReserva();
        OperacionStub operacionStub = new OperacionStub(BD);
        OperacionFactory.setOperacionBO(operacionStub);
        try {
            reserva.realizaReserva(login, password, socio, isbns);
        } catch (ReservaException e) {
            resultado = e;
        }
        if (resultadoEsperado == null) {
            assertNull(resultado);
        } else {
            assertEquals(resultadoEsperado.getMessage(), resultado.getMessage());
        }
    }

}