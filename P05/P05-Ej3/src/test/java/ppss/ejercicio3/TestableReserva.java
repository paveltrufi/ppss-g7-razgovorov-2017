package ppss.ejercicio3;

import java.util.Arrays;
import java.util.List;

public class TestableReserva extends Reserva {
    List<String> logins = Arrays.asList("ppss");
    List<String> passwords = Arrays.asList("ppss");

    @Override
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
        return tipoUsu.equals(Usuario.BIBLIOTECARIO) && logins.contains(login) && passwords.contains(password);
    }
}
