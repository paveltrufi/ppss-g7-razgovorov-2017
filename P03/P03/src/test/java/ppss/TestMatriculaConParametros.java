package ppss;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Category(ConParametros.class)
@RunWith(Parameterized.class)
public class TestMatriculaConParametros {
    private int edad;
    private boolean familiaNumerosa, repetidor;
    private float tasaEsperada;
    private Matricula matricula = new Matricula();

    public TestMatriculaConParametros(int edad, boolean familiaNumerosa, boolean repetidor, float tasaEsperada) {
        this.edad = edad;
        this.familiaNumerosa = familiaNumerosa;
        this.repetidor = repetidor;
        this.tasaEsperada = tasaEsperada;
    }

    @Parameterized.Parameters(name = "C{index}: calculaTasaMatricula({0}, {1}, {2}) = {3}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {18, false, true, 2000.00f},
                {18, false, false, 500.00f},
                {18, true, false, 250.00f},
                {65, false, false, 250.00f},
                {60, false, false, 400.00f},
                {60, true, false, 400.00f}
        });
    }

    @Test
    public void calculaTasaMatriculaConParametrosTest() {
        assertThat(matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor), is(tasaEsperada));
    }
}