package ppss;

import java.util.StringJoiner;


public class DataArray {
    private int[] coleccion;
    private int numElem;
    
    //Constructor
    public DataArray() {
        coleccion = new int[10];
        numElem=0;
    }
    
    public int size() {
        return numElem;
    }
    
    //método para añadir un entero a la colección
    public void add(int elem) {
    if (numElem < (coleccion.length)) {
            coleccion[numElem]= elem;
            numElem++;
            System.out.println("added "+elem +" ahora hay "+numElem+ " elementos");
        } else {
            System.out.println(elem +" ya no cabe. Ya has añadido "+numElem+" elementos");
        } 
    }
    
    //método para borrar un entero a la colección
    /**
     * Borra todas las ocurrencias de un elemento presentes en la colección
     * @param elem el elemento a borrar
     * @return la colección resultante
     */
    public int[] delete(int elem) {
        for(int i = 0; i < numElem; i++) {
            if(coleccion[i] == elem) {
                //System.out.println("Antes  : " + coleccionToString());
                for(int j = i; j < numElem-1; j++) {
                    coleccion[j] = coleccion[j+1];
                }
                coleccion[--numElem] = 0;
                i--;
                //System.out.println("Después: " + coleccionToString());
            }
        }
        return coleccion;
    }
    
    /**
     * Genera la colección de números separados por comas
     * @return la cadena generada
     */
    private String coleccionToString() {
        StringJoiner joiner = new StringJoiner(", ");
        for (int item : coleccion) joiner.add(Integer.toString(item));
        return joiner.toString();
    }
}
